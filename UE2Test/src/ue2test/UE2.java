package ue2test;

import javacard.framework.APDU;
import javacard.framework.ISO7816;
import javacard.framework.Applet;
import javacard.framework.ISOException;
import javacard.framework.Util;

/**
 * @author Gerald Madlmayr
 */

public class UE2 extends Applet {

	// |Calcu
	
	// Constants for Instructions
	private final static byte ADD = 0x01;
        private final static byte SUBTRACT = 0x02;
        private final static byte MULTIPLY = 0x03;
        
        private final static byte AND = 0x05;
        private final static byte OR = 0x06;
        private final static byte NOT = 0x07;
        
	private final static byte SEPARATOR = (byte)0x80; 

	public static void install(byte[] bArray, short bOffset, byte bLength) {
		// GP-compliant JavaCard applet registration          
		//new UE2().register(bArray, (short) (bOffset + 1),
		//		bArray[bOffset]);
                new UE2().register();
	}

	/**
	 * Helper Function to Convert a Short to Byte Array
	 * 
	 * @param s
	 *            Short which needs to be converted to a byte array
	 * @return byte with len of 2 containing the high and low byte of the short.
	 */

	public static byte[] toBytes(short s) {
		// Causes a memory leak by allocating memory on the heap EVERY time it
		// is called
		// You should shift right then mask
		// byte highx = (byte) ((s & 0xFF00) >> 8);

		// 1. Shift by 8
		// 2. Cast to byte
		// 3. Mask as Byte
		// 4. Cast to byte
		byte high = (byte) ((byte) (s >> 8) & 0xff);
		byte low = (byte) (s & 0xff);
		return new byte[] { high, low };
	}

	public void process(APDU apdu) {
		// Good practice: Return 9000 on SELECT
		if (selectingApplet()) {
			return;
		}
		
		short result = 0;
		byte[] buf = apdu.getBuffer();              
                
		
		// check, if the 8 bit for "signed" bytes is set, as in this case
		// the byte would be negative. in this case we throw an 
		// excepction
                if((buf[ISO7816.OFFSET_P1] & SEPARATOR) == SEPARATOR || (buf[ISO7816.OFFSET_P2] & SEPARATOR) == SEPARATOR)
			ISOException.throwIt(ISO7816.SW_INCORRECT_P1P2);
		
		switch (buf[ISO7816.OFFSET_INS]) {
		case (byte) ADD:
			result = (short) ((short)(buf[ISO7816.OFFSET_P1]) + (short)(buf[ISO7816.OFFSET_P2]));
			break;    
                
                case (byte) SUBTRACT:
                        if((short)buf[ISO7816.OFFSET_P1] > (short)(buf[ISO7816.OFFSET_P2])) {
                            result = (short) ((short)(buf[ISO7816.OFFSET_P1]) - (short)(buf[ISO7816.OFFSET_P2]));
                        } else {
                            result = 0;
                        }
			break;  
                    
                case (byte) MULTIPLY:			
                    result = (short) ((short)(buf[ISO7816.OFFSET_P1]) * (short)(buf[ISO7816.OFFSET_P2]));		
                    break;  
                    
                case (byte) AND:			
                    result = (short) ((short)(buf[ISO7816.OFFSET_P1]) & (short)(buf[ISO7816.OFFSET_P2]));		
                    break;
                    
                case (byte) OR:			
                    result = (short) ((short)(buf[ISO7816.OFFSET_P1]) | (short)(buf[ISO7816.OFFSET_P2]));		
                    break;
                    
                 case (byte) NOT:			
                    result = (short) ((short)(~buf[ISO7816.OFFSET_P1]));		
                    break;
		default:
			// good practice: If you don't know the INStruction, say so:
			ISOException.throwIt(ISO7816.SW_INS_NOT_SUPPORTED);
		}
                
                // copy short to outbuffer; no short, but bytes
                Util.arrayCopyNonAtomic(toBytes(result), (short) 0, buf, (short) 0, (short) 2);                       

                // set lengh of outbuffer and flush.
                apdu.setOutgoingAndSend((short) 0, (short) 2);
	}

}