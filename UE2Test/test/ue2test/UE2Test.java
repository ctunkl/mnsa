/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ue2test;

import com.licel.jcardsim.io.CAD;
import com.licel.jcardsim.io.JavaxSmartCardInterface;
import javacard.framework.AID;
import javax.smartcardio.CommandAPDU;
import javax.smartcardio.ResponseAPDU;

import org.junit.AfterClass;
import org.junit.Assert.*;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import org.junit.BeforeClass;
import org.junit.Test;


/**
 *
 * @author christophertunkl
 */
public class UE2Test {
    public static JavaxSmartCardInterface simulator;
   
    @Test
    public void testAdd() {       
        apduAddTest("05", "06", "000B");
        apduAddTest("00", "00", "0000");
        apduAddTest("79", "79", "00F2");
    } 
    
    @Test
    public void testSubtract() {       
        apduSubtractTest("07", "05", "0002");
        apduSubtractTest("05", "07", "0000");
        apduSubtractTest("68", "68", "0000");
        apduSubtractTest("00", "00", "0000");
        apduSubtractTest("4A", "2B", "001F");
        
    } 
    
    
    @Test
    public void testMultiply() {       
        apduMultiplyTest("05", "06", "001E");
        apduMultiplyTest("00", "00", "0000");
        apduMultiplyTest("0F", "0D", "00C3");
        apduMultiplyTest("0F", "00", "0000");
        apduMultiplyTest("68", "68", "2A40");
    } 
    
    @Test
    public void testAnd() {       
        apduAndTest("05", "06", "0004");
        apduAndTest("05", "00", "0000");
        apduAndTest("1F", "2D", "000D");        
        apduAndTest("68", "68", "0068");
    } 
    
    @Test
    public void testOr() {       
        apduOrTest("05", "06", "0007");
        apduOrTest("05", "00", "0005");
        apduOrTest("1F", "2D", "003F");
        apduOrTest("13", "34", "0037");
        apduOrTest("68", "68", "0068");
    } 
    
    @Test
    public void testNot() {       
        apduNotTest("05", "FFFA");
        apduNotTest("00", "FFFF");
        apduNotTest("3F", "FFC0");
        apduNotTest("77", "FF88");
        apduNotTest("68", "FF97");
    } 
      
    
    
   public void apduAddTest(String p1, String p2, String expected) {
       apduTest("00", "01", p1, p2, "00", expected);
   }
   
   public void apduSubtractTest(String p1, String p2, String expected) {
       apduTest("00", "02", p1, p2, "00", expected);
   }
   
   public void apduMultiplyTest(String p1, String p2, String expected) {
       apduTest("00", "03", p1, p2, "00", expected);
   }
   
   public void apduAndTest(String p1, String p2, String expected) {
       apduTest("00", "05", p1, p2, "00", expected);
   }
   
   public void apduOrTest(String p1, String p2, String expected) {
       apduTest("00", "06", p1, p2, "00", expected);
   }
   
   public void apduNotTest(String p1, String expected) {
       apduTest("00", "07", p1, "00", "00", expected);
   }
   
      
   public void apduTest(String cla, String op, String p1, String p2, String len, String expected) {
        CommandAPDU command = new CommandAPDU(Tools.hexToBytes(cla+op+p1+p2+len));
        ResponseAPDU response = simulator.transmitCommand(command);

        System.out.println("Response = " + Tools.bytesToHex(response.getBytes()));

        assertEquals(0x9000, response.getSW());
        assertArrayEquals(Tools.hexToBytes(expected), response.getData()); 
   }
      
    
   @BeforeClass
   public static void setUp() {
        System.setProperty("com.licel.jcardsim.terminal.type", "2");
        CAD cad = new CAD(System.getProperties());

        UE2Test.simulator = (JavaxSmartCardInterface) cad.getCardInterface();
        byte [] appletAIDBytes = new byte[]{1,2,3,4,5,6,7,8,9};
        AID appletAID =new AID(appletAIDBytes,(short)0,(byte) appletAIDBytes.length);

        UE2Test.simulator.installApplet(appletAID, UE2.class); 
        UE2Test.simulator.selectApplet(appletAID);
   }
   
   @AfterClass
   public static void tearDown() {
       UE2Test.simulator = null;
   }    
}
