/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ue3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 *
 * @author christophertunkl
 */
public class Sender {
    
    private SerialConnection serialConnection = null;
    private BufferedReader br = null;
    private BufferedWriter bw = null;
    
    public Sender(String port) throws Exception {        
        this.serialConnection = new SerialConnection(port);
        this.serialConnection.open();
        this.br = new BufferedReader(new InputStreamReader(this.serialConnection.getInputStream()));
        this.bw = new BufferedWriter(new OutputStreamWriter(this.serialConnection.getOutputStream()));        
    }
    
    public void send(SMS sms) {
        // AT commands senden, sms in byte-array transformieren. dabei auch 7-bit-zeugs achten.        
        // laut tuwel sollen sms auch wenn nötig auf mehrere teile aufgesplittet werden
    }
    
    public void send(String message) throws IOException {      
        System.out.println("Sendding: "+message);
                            
        this.bw.write(message + (char)13);
        this.bw.flush();
   }
    
    public String receive() throws IOException {       
        String response = "";
        String line;
        
        do {
            line = this.br.readLine();
            response += line;
        } while(line.contains("OK") == false && line.contains("ERROR") == false);
                
        System.out.println("Received: " + response);
        return response;             
    }  
}
