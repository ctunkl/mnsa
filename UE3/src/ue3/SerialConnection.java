package ue3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.comm.CommPortIdentifier;
import javax.comm.SerialPort;



public class SerialConnection {
	
    private SerialPort serialPort = null;
    private String portIdentifier = "COM3";

    private InputStream comInput = null;
    private OutputStream comOutput = null;

    public SerialConnection() {	
    }

    public SerialConnection(String portIdentifier) {
        this.portIdentifier = portIdentifier;
    }


    public String getPortName() {
        return this.portIdentifier;
    }

    public void open() throws Exception {	
        this.openSerialPort();
        this.openStreams();		
    }

    public void close() throws IOException {
        this.serialPort.close();
        this.comInput.close();
        this.comOutput.close();
    }

    @SuppressWarnings("unchecked")
    private void openSerialPort() throws Exception {
        CommPortIdentifier portId;        
        Enumeration<CommPortIdentifier> portList = CommPortIdentifier.getPortIdentifiers();        

        while (portList.hasMoreElements()) {
            portId = portList.nextElement();
            System.out.println(portId);
            
            if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL && portId.getName().equals(this.portIdentifier)) {              
                this.serialPort = (SerialPort)portId.open(this.getClass().getSimpleName(), 2000);
               // this.serialPort.setSerialPortParams(9600,SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);
                return;
            }
        }

        throw new Exception("NO PORT");
    }

    private void openStreams() throws IOException {
        this.comInput = this.serialPort.getInputStream();		
        this.comOutput = this.serialPort.getOutputStream();
    }

    public InputStream getInputStream() {
        return this.comInput;
    }

    public OutputStream getOutputStream() {
        return this.comOutput;
    }
}
