/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ue3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 *
 * @author christophertunkl
 */
public class UE3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Properties properties = loadProperties("sendsms.properties");
            String csvFile = properties.getProperty("csvFile");
            String port = properties.getProperty("port");
            
            List<SMS> lines = readCSV(csvFile);
            for(SMS sms : lines) {
                System.out.println("SMS: " + sms.recipient + ", "+sms.text);
            }
            Sender sender = new Sender(port);
            sender.send("AT");
            System.out.println("sent");
            sender.receive();
            System.out.println("Received.");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    
    private static Properties loadProperties(String fileName) throws IOException {
        Properties prop = new Properties();
        InputStream in = UE3.class.getResourceAsStream(fileName);        
        prop.load(in);
        in.close();             
        return prop;
    }
    
    private static List<SMS> readCSV(String csvFile) throws IOException {
       List<SMS> list = new ArrayList<SMS>();
    
        BufferedReader br = new BufferedReader(new FileReader(csvFile));
        String line;
        String [] fields;

        while((line = br.readLine()) != null) {
            fields = line.split(";");
            list.add(new SMS(fields[0], fields[1]));
        }

        br.close();
       
        return list;
    }
}
