/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mnsa.clientapp;

import java.util.ArrayList;
import java.util.List;

import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;
import javax.smartcardio.ResponseAPDU;

import msna.communication.Message;
import msna.nokiaprovider.ECardReader;
import msna.tools.Tools;

import org.apache.log4j.BasicConfigurator;

@SuppressWarnings("restriction")
public class MainClass {
	
	public static void main(String[] args) throws Exception {
    	BasicConfigurator.configure();
     
    	ECardReader reader = new ECardReader();
    	
    	reader.open();	    	      
    	
    	if(reader.isCardPresent()){
			System.out.println("\n--- get ATR ---");
			System.out.println(new String(reader.getAtr().getBytes()));
			
			System.out.println("\n--- get Name ---");
			System.out.println(reader.getName());			
														
			ResponseAPDU response = sendPrint(reader, "Select Card Manager", ECardReader.SELECT_CARD_MANAGER_APDU);					
									
			
			System.out.println("\nClosing connection.");
			reader.close();
		} else {
			System.out.println("No Card Connected");
		}      	
    }
	
	public static ResponseAPDU sendPrint(ECardReader reader, String debug, CommandAPDU apdu) throws CardException {
		System.out.println("\n> "+debug+": " + Tools.bytesToHex(apdu.getBytes()));
		ResponseAPDU response = reader.transceive(apdu);
		System.out.println("Response: " + Tools.bytesToHex(response.getBytes()));
		return response;
	}		
}