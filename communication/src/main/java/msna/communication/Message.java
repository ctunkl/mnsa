package msna.communication;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

import msna.tools.Tools;

public class Message {
	public byte type;
	public byte node;
	public byte[] payload;
	
	private static final int HEADER_SIZE = 4;
	
	public static final byte TYPE_OPEN = 0x01;
	public static final byte TYPE_CLOSE = 0x02;
	public static final byte TYPE_CONNECTED = 0x03;
	public static final byte TYPE_APDU = 0x04;
	public static final byte TYPE_UID = 0x05;
	
	public static final byte NODE_PHONE = 0x055;
	public static final byte NODE_PC = 0x066;
	
	
	public Message(byte type, byte node) {
		this(type, node, new byte[]{});
	}
	
	public Message(byte type, byte node, byte [] payload) {
		this.type = type;
		this.node = node;
		this.payload = payload;
	}
	
	public Message(byte[] bytes) {
		if(bytes == null)
			throw new IllegalArgumentException("bytes = null");
		
		if(bytes.length < 2)
			throw new IllegalArgumentException("bytes too short");
		
		int payloadLength = bytes[2]*256 + bytes[3];
		
		if(bytes.length != 4 + payloadLength)
			throw new IllegalArgumentException("bytes wrong format");
		
		
        this.type = bytes[0];
        this.node = bytes[1];       
        this.payload = new byte[payloadLength];
                
        for(int i=Message.HEADER_SIZE; payloadLength>0; payloadLength--, i++) {
            this.payload[i-Message.HEADER_SIZE] = bytes[i];
        }
	}
	
	public Message(byte [] header, byte [] payload) {
		this(header[0], header[1], payload);
	}
	
	public byte[] toBytes() {
		int payloadLength = 0;
		
		if(this.payload != null)
			payloadLength = this.payload.length;
		
		byte [] bytes = new byte[Message.HEADER_SIZE + payloadLength];
		bytes[0] = this.type;
		bytes[1] = this.node;
		bytes[2] = (byte)(payloadLength/256);
		bytes[3] = (byte)(payloadLength%256);
		
		if(this.payload != null) {
			int i = Message.HEADER_SIZE;
			for(byte b : this.payload) {
				bytes[i] = b;
				i++;
			}
		}
		
		return bytes;
	}	
	
	public static void send(Message m, OutputStream out) throws IOException {		
		System.out.println("Sending " + m.toString());
		
		out.write(m.toBytes());
		out.flush();
	}
	
	public static Message receive(InputStream in) throws IOException {
		byte [] header = new byte[Message.HEADER_SIZE];		
		int read = 0;
		
		while(read < Message.HEADER_SIZE) {
			read += in.read(header, read, Message.HEADER_SIZE-read);
		}	
		
		int payloadLength = header[2]*256 + header[3];

		byte [] payload = new byte[payloadLength];
		read = 0;
		while(read < payloadLength) {
			read += in.read(payload, read, payloadLength-read);
		}
		
		Message response = new Message(header, payload);
		System.out.println("Received "+response.toString());
		
		return response; 
	}
	
	public String toString() {
		byte [] bytes = this.toBytes();
		String s = "";
		s += ("Type = "+print(bytes, 0,0)+", ");
		s += ("Node = "+print(bytes, 1,1)+", ");
		s += ("HiLen = "+print(bytes,2,2)+", ");
		s += ("LowLen = "+print(bytes,3,3)+", ");
		s += ("Payload = "+print(bytes,4,bytes.length-1).toUpperCase()+" (totalLen = ");
		int payloadLength = bytes[2]*256 + bytes[3];
		s += (payloadLength+")");
		
		return s;
	}
	
	private String print(byte [] bytes, int begin, int end) {
		return Tools.bytesToHex(Arrays.copyOfRange(bytes, begin, end+1));
	}
}
