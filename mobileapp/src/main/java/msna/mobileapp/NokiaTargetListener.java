/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package msna.mobileapp;

import javax.microedition.contactless.TargetListener;
import javax.microedition.contactless.TargetProperties;
import javax.microedition.contactless.TargetType;
import javax.microedition.contactless.sc.ISO14443Connection;
import javax.microedition.io.Connector;



/**
 *
 * @author christophertunkl
 */
public class NokiaTargetListener implements TargetListener {

      public ISO14443Connection connection = null;
      public TargetProperties targetProperties = null;
      
      public NokiaTargetListener() {
         Tracer.outln("init");
      }
      
      
      public byte[] exchangeData(byte[] data) {
          try {
              Tracer.outln("SENT TO CARD = "+Tools.bytesToHex(data));
              Thread.sleep(3000);
              return this.connection.exchangeData(data);
          } catch (Exception ex) {
              Tracer.ex(ex);
          } 
          return new byte[0];
      }
      
      
      public boolean isConnectionReady() {
          return this.connection != null;       
      }
      
      public boolean isCardPresent() {
    	  return this.targetProperties != null;
      }
    
    
      public void targetDetected(TargetProperties[] targetProperties) {
          TargetProperties tp;
          String url;
          
          for (int i=0; i < targetProperties.length; i++) {
            tp = targetProperties[i];

            if (tp.hasTargetType(TargetType.ISO14443_CARD)) {
                try {
                    url = tp.getUrl(Class.forName("javax.microedition.contactless.sc.ISO14443Connection"));
                    
                    Tracer.outln("Target detected!");
                    Tracer.outln("UID = " + tp.getUid());
                    this.connection = (ISO14443Connection)Connector.open(url);
                    this.targetProperties = tp;
                    
                    return;                
                } catch (Exception ex) {
                    Tracer.ex(ex);
                }
            }            
        }
    } 
}
