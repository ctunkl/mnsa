/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package msna.mobileapp;

import javax.microedition.lcdui.TextBox;


/**
 *
 * @author christophertunkl
 */
public class Tracer {
        private static TextBox textbox;
        
        public static void init(TextBox textbox) {
            Tracer.textbox = textbox;
        }
        
        private Tracer() {}

        public static void outln (String msg) {
            textbox.setString((textbox.getString()).concat(msg + "\n"));           
        }

        public static void out (String msg) {
            textbox.setString((textbox.getString()).concat(msg));
        }
        
        public static void ex (Exception e) {
            Tracer.outln(e.getClass().getName()+": ".concat(e.getMessage()));
        }
}