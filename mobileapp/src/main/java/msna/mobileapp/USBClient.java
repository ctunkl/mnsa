package msna.mobileapp;


import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.microedition.contactless.DiscoveryManager;
import javax.microedition.contactless.TargetType;
import javax.microedition.io.CommConnection;
import javax.microedition.io.Connector;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.TextBox;
import javax.microedition.midlet.MIDlet;



/**
 * MIDlet reads System properties Comm ports, opens CommConnection to port COM0
 * attempts to get and set Baud rate, writes text string over CommConnection
 * then listens and displays bytes received from CommConnection.
 */
public class USBClient extends MIDlet implements CommandListener {
    private Command startCommand;
    private Command exitCommand;
    private Display display;
    private TextBox textbox;
    
    private CommConnection comm = null;
    private InputStream is = null;
    private OutputStream os = null;
    private Thread thread = null;

    private NokiaTargetListener targetListener = null;
    
    public USBClient() {
        display = Display.getDisplay(this);
        startCommand = new Command ("Start", Command.SCREEN, 1);
        exitCommand = new Command("Exit", Command.EXIT, 1);
    }

    /**
     * Start up the MIDlet by creating the TextBox and associating
     * the exit command and listener.
     */
    public void startApp() {
        textbox = new TextBox("USBTest", "", 8000, 0);
        textbox.addCommand(startCommand);
        textbox.addCommand(exitCommand);
        textbox.setCommandListener(this);
        display.setCurrent(textbox);
        
        Tracer.init(textbox);
       
        targetListener = new NokiaTargetListener();
             
        try {
            DiscoveryManager.getInstance().addTargetListener(targetListener,TargetType.ISO14443_CARD);
        } catch (Exception ex) {
            Tracer.ex(ex);
        } 
        
        
        
        openUSBConnection();
    }

    public void pauseApp() { }

    /**
     * Destroy must cleanup everything not handled by the garbage collector.
     */
    public void destroyApp(boolean unconditional) { }

    /*
     * Respond to commands, including exit
     * On the exit command, cleanup and notify that the MIDlet has been destroyed.
     */
    public void commandAction(Command c, Displayable s) {
        if (c == startCommand) {
            startListening();
        }
        if (c == exitCommand) {
            Tracer.outln("Exiting...");
            notifyDestroyed();
        }
    }
    
    private void startListening() {
        if(thread == null) {
            thread = new Thread() {
                    public void run() {
                        listenUSB();
                    }
                };
            thread.start();
        }
    }

    public void openUSBConnection() {
        try {
            Tracer.outln("USasdasdBTest: Testing CommConnection for USB ports");
         
            String ports = System.getProperty("microedition.commports");
            int index = ports.indexOf("USB", 0);
            if(index == -1) {
                throw new RuntimeException("No USB port found in the device");
            }           
            
            comm = (CommConnection)Connector.open("comm:"+ports);                       
            os = comm.openOutputStream();
            is = comm.openInputStream();
            
            startListening();
    
           /* String text = "Hello.\r\n";
            os.write(text.getBytes());
            os.flush();*/
        } catch (IOException e) {
            Tracer.ex(e);
        }
    }

    public void listenUSB() {
        Tracer.outln("Listening USB port...\n");
                
        boolean quit = false;
        
        try {           
           while(!quit) {
                try {
                    Thread.sleep(10);
                }
                catch(InterruptedException ie) { }
                int available = is.available();
                if(available == 0) {
                    continue;
                }                
                                
                Message m = Message.receive(is);
                Message response = null;
                
                Tracer.outln("Message: " + m);                
                                                
                switch(m.type) {
                	case Message.TYPE_UID:
                		response = new Message(Message.TYPE_UID, Message.NODE_PC, this.targetListener.targetProperties.getUid().getBytes());
                		break;
                	case Message.TYPE_APDU:
                		response = new Message(Message.TYPE_APDU, Message.NODE_PC, this.targetListener.exchangeData(m.payload));
                		break;    
                	case Message.TYPE_CONNECTED:
                		response = new Message(Message.TYPE_CONNECTED, Message.NODE_PC, 
                				        new byte[]{ (byte) (this.targetListener.isConnectionReady() ? 0x01 : 0x00)});
                		break;
                	case Message.TYPE_CLOSE:                	
                		quit = true;                		
                		this.targetListener.connection.close();
                		break;
                }
                                                               
                Tracer.outln("Response: " + response);
                if(response != null)
                	Message.send(response, os);
                Tracer.outln("Response sent.\n");                                                                                                               
            }
            Tracer.outln("Calling OutputStream.close()");
            if(os != null)
            	os.close();
            Tracer.outln("Calling InputStream.close()");
            if(is != null)
            	is.close();
            Tracer.outln("Calling connection.close()");
            if(comm != null)
            	comm.close();
        }
        catch (Exception ioe) {
            Tracer.ex(ioe);
        }
        Tracer.outln("Closed everything.");
    }
}
