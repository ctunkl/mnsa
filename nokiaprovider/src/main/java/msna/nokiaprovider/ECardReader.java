package msna.nokiaprovider;


import java.security.Security;
import java.util.List;

import javax.smartcardio.ATR;
import javax.smartcardio.CardException;
import javax.smartcardio.CardTerminal;
import javax.smartcardio.CommandAPDU;
import javax.smartcardio.ResponseAPDU;
import javax.smartcardio.TerminalFactory;

import org.apache.log4j.Logger;



import msna.communication.Message;
import msna.tools.Tools;


@SuppressWarnings("restriction")
public class ECardReader {
	//private static byte[] SELECT ={(byte) 0x00, (byte) 0xA4, (byte) 0x04, (byte) 0x0C, (byte) 0x08, (byte)0xD0, (byte)0x40, (byte)0x00, (byte)0x00, (byte)0x17, (byte)0x01, (byte)0x01, (byte)0x01};
    private static byte[] SELECT_MASTER_FILE ={(byte) 0x00, (byte) 0xA4, (byte) 0x04, (byte) 0x00, (byte) 0x02, (byte)0x3F, (byte)0x00}; 	     	   
    public static CommandAPDU SELECT_MASTER_FILE_APDU = new CommandAPDU(SELECT_MASTER_FILE);

    private static byte[] READ_BINARY = {(byte)0x00, (byte)0xB0, (byte)0x82, (byte)0x00, (byte) 0x00};
    public static CommandAPDU READ_BINARY_APDU = new CommandAPDU(READ_BINARY);
    
    private static byte[] SELECT_CARD_MANAGER ={(byte) 0x00, (byte) 0xA4, (byte) 0x04, (byte) 0x00, (byte) 0x08, (byte) 0xA0 , (byte) 0x00 , (byte) 0x00 , (byte) 0x00 , (byte) 0x03 , (byte) 0x00 , (byte) 0x00 , (byte) 0x00, (byte) 0x00}; 	     	   
    public static CommandAPDU SELECT_CARD_MANAGER_APDU = new CommandAPDU(SELECT_CARD_MANAGER);

    private final static String T_1 ="T=1";
    private final static String T_0 ="T=0";
    
    private NokiaTerminal cardTerminal = null;
    private NokiaCard card = null;
    
    public ECardReader() {    	
    }
    
    // Logger
	protected final static Logger logger = Logger.getLogger(ECardReader.class);

    public void open() throws Exception {    	    	
    	if(Security.getProvider("NokiaProvider") == null) {
    		Security.addProvider(new NokiaProvider());
    	}
    	
        TerminalFactory tf = TerminalFactory.getInstance("NokiaProvider", null);
        NokiaTerminals nts = (NokiaTerminals)tf.terminals();
        
        List<CardTerminal>list = nts.list();
        
        for(CardTerminal ct : list) {
        	if(ct.getName() == "NokiaTerminal.Terminal") {
        		cardTerminal = (NokiaTerminal)ct;
        		break;
        	}
        }
         
        if(cardTerminal != null)
        	logger.info("Terminal '" + cardTerminal.getName() + "' fetched");
        else{
        	logger.error("Error - Terminal not available; exit");
        	return; 
        }	
        
        try {
            while (cardTerminal.isCardPresent()) {
                try {
                    Thread.sleep(1000);
                } catch (Exception e) {

                }

                try {
                    card = (NokiaCard) cardTerminal.connect(T_1);
                    logger.info("Terminal connected mit T=1");
                } catch (Exception e) {
                	 logger.error("Terminal NOT connected via T=1 " + e.toString());
                	 
                     try {
                         card = (NokiaCard) cardTerminal.connect(T_0);
                         logger.info("Terminal connected mit T=0");
                     } catch (Exception ex) {
                     	 logger.error("Terminal NOT onnected via T=0 " + ex.toString());                     	
                     }                	  
                }                

                if(card != null)
                	logger.info("Card okay");              
                else                
                	logger.error("Card is NULL!");  
                
                return;
            }
        }
        catch (CardException e) {
        	logger.info("Error isCardPresent()" + e.toString());
        }
    }
    
    public void close() throws CardException {
    	this.card.disconnect(true);
    	this.card = null;
    }
    
    
    public ResponseAPDU transceive(CommandAPDU apdu) throws CardException {
    	NokiaChannel ch = (NokiaChannel) card.getBasicChannel();
    	
    	return ch.transmit(apdu);
    }   
    
    public boolean isCardPresent() {
    	return (this.card != null);
    }
    
    public ATR getAtr() {
    	return this.card.getATR();
    }
    
    public String getName() {
    	return this.cardTerminal.getName();
    }
    
    

    // wenn eine Reponse auf 0x9000 Endet, ist alles gut.
    public static boolean check9000(ResponseAPDU ra) {
        byte[] response = ra.getBytes();
        return (response[response.length - 2] == (byte) 0x90 && response[response.length - 1] == (byte) 0x00);
    }

  
    // wenn eine Reponse auf 0x9000 Endet, ist alles gut.
    public static boolean check9000andPrint(ResponseAPDU ra) {
        byte[] response = ra.getBytes();
        logger.info(Tools.bytesToHex(response));
        return (response[response.length - 2] == (byte) 0x90 && response[response.length - 1] == (byte) 0x00);
    }
    
    
    
}
