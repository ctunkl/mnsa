package msna.nokiaprovider;

import java.io.IOException;

import javax.smartcardio.ATR;
import javax.smartcardio.Card;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;
import javax.smartcardio.ResponseAPDU;

import msna.communication.Message;


/**
 * Card implementation class.
 */

public class NokiaCard extends Card {
	// default protocol
	private static final String T0_PROTOCOL = "T=0";
	// default ATR - NXP JCOP 31/36K
	private static final String DEFAULT_ATR = "3BFA1800008131FE454A434F5033315632333298";
	
	private ATR atr;

	private NokiaChannel basicChannel;
	
	private SerialConnection serialConnection;

	public NokiaCard(SerialConnection serialConnection) {
		atr = new ATR(DEFAULT_ATR.getBytes());
		basicChannel = new NokiaChannel(this, 0);		
		this.serialConnection = serialConnection;
	}
	
	public void open() throws Exception {
		
	}
			
	@Override
	public ATR getATR() {
		try {
			atr = this.serialConnection.getATR();			
		} catch (IOException e) {			
		}
		
		return this.atr;
	}

	/**
	 * Always returns T=0.
	 */
	@Override
	public String getProtocol() {
		return T0_PROTOCOL;
	}

	@Override
	public CardChannel getBasicChannel() {
		return basicChannel;
	}

	/**
	 * Always returns basic channel with id = 0
	 * 
	 * @throws CardException
	 */
	@Override
	public CardChannel openLogicalChannel() throws CardException {
		return basicChannel;
	}
	
	@Override
	public void beginExclusive() throws CardException {
	}

	@Override
	public void endExclusive() throws CardException {
	}

	@Override
	public byte[] transmitControlCommand(int i, byte[] bytes)
			throws CardException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public void disconnect(boolean bln) throws CardException {
		try {
			Message.send(new Message(Message.TYPE_CLOSE, Message.NODE_PHONE), serialConnection.getOutputStream());
		} catch (IOException e) {			
			e.printStackTrace();
		}
		try {
			this.serialConnection.close();
		} catch (IOException e) {			
			e.printStackTrace();
		}
	}

		
	public ResponseAPDU transmitCommand(CommandAPDU capdu) {								
		Message m = new Message(Message.TYPE_APDU, Message.NODE_PHONE, capdu.getBytes());			
		Message response = null;
		
		try {
			Message.send(m, serialConnection.getOutputStream());
			response = Message.receive(serialConnection.getInputStream());
		} catch (IOException e) {			
			e.printStackTrace();
		}											
	
		return new ResponseAPDU(response.payload);				
	}		
}
