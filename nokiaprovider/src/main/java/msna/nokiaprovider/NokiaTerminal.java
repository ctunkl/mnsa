package msna.nokiaprovider;

import java.io.IOException;

import javax.smartcardio.Card;
import javax.smartcardio.CardException;
import javax.smartcardio.CardTerminal;

import msna.communication.Message;

/**
 * CardTerminal implementation class.
 */
public class NokiaTerminal extends CardTerminal {

    public final static String NAME="NokiaTerminal.Terminal";
    private static NokiaCard card = null;
    
    private SerialConnection serialConnection;
    
    public NokiaTerminal() {
    	this.serialConnection = new SerialConnection();
    }
            
    public String getName() {
        return NAME;    
    }

    public Card connect(String string) {    	    	    	
        if (card == null) {        	                        
            try {
            	this.serialConnection.open();
            	card = new NokiaCard(this.serialConnection);
				card.open();				
			} catch (Exception e) {				
				e.printStackTrace();
			}
        }
        return card;
    }

    /**
     * Always returns true
     */
    public boolean isCardPresent() throws CardException {
        try {
			Message.send(new Message(Message.TYPE_CONNECTED, Message.NODE_PHONE), this.serialConnection.getOutputStream());
		} catch (IOException e) {
			return false;			
		}
        Message response = null;
        try {
			response = Message.receive(this.serialConnection.getInputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return false;
		}
        
        return response.payload.length == 1 && response.payload[0] == 0x01;
    }

    /**
     * Immediately returns true
     */
    public boolean waitForCardPresent(long l) throws CardException {
        return true;
    }

    /**
     * Immediately returns true
     */
    public boolean waitForCardAbsent(long l) throws CardException {
        return false;
    }
}
