package msna.nokiaprovider;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;

import javax.comm.CommPortIdentifier;
import javax.comm.SerialPort;
import javax.smartcardio.ATR;

import msna.communication.Message;


public class SerialConnection {
	
	private SerialPort serialPort = null;
	private final String portIdentifier = "COM3";
	
	private InputStream comInput = null;
	private OutputStream comOutput = null;
	
	public SerialConnection() {				
	}
	
	public String getPortName() {
		return this.portIdentifier;
	}
	
	public void open() throws Exception {	
		this.openSerialPort();
		this.openStreams();		
	}
	
	public void close() throws IOException {
		this.serialPort.close();
		this.comInput.close();
		this.comOutput.close();
	}
	
	@SuppressWarnings("unchecked")
	private void openSerialPort() throws Exception {
        CommPortIdentifier portId;
        
		Enumeration<CommPortIdentifier> portList = CommPortIdentifier.getPortIdentifiers();        
        while (portList.hasMoreElements()) {
            portId = portList.nextElement();
            System.out.println(portId);
            if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL && portId.getName().equals(this.portIdentifier)) {              
            	this.serialPort = (SerialPort)portId.open(this.getClass().getSimpleName(), 1000);
				this.serialPort.setSerialPortParams(9600,SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);
                return;
            }
        }
        
        throw new Exception("NO PORT");
	}
	
	private void openStreams() throws IOException {
		this.comInput = this.serialPort.getInputStream();		
		this.comOutput = this.serialPort.getOutputStream();
	}
	
	public InputStream getInputStream() {
		return this.comInput;
	}
	
	public OutputStream getOutputStream() {
		return this.comOutput;
	}	
	
	public ATR getATR() throws IOException {		
		Message.send(new Message(Message.TYPE_UID, Message.NODE_PHONE), this.comOutput);
		Message response = Message.receive(this.comInput);
			
		return new ATR(response.payload);				
	}
}
