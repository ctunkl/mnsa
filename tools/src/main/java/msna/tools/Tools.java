package msna.tools;

import java.util.Arrays;

public class Tools {
	public static String bytesToHex(byte[] data) {
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < data.length; i++) {
            String bs = Integer.toHexString(data[i] & 0xFF);
            if (bs.length() == 1) {
                sb.append(0);
            }
            sb.append(bs);
            
        }

        return sb.toString();
    }

	public static byte[] hexToBytes(String receiveMessage) {
		byte[] bytes = new byte[receiveMessage.length()/2];	
		
		for(int i=0; i<receiveMessage.length()-1; i+=2) {
			bytes[i/2] = (byte) (Character.digit(receiveMessage.charAt(i), 16)*16 + Character.digit(receiveMessage.charAt(i+1), 16));			
		}
		return bytes;
	}
	
	public static String printBytes(byte [] bytes) {
		String s = null;
		
		for(byte b : bytes) {
			if(s == null) 
				s = "" + b;
			else
				s += ", " + b;
		}
	
		return s+"\n";
	}	
}
